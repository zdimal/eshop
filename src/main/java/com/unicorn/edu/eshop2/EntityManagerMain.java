package com.unicorn.edu.eshop2;

import com.unicorn.edu.eshop2.repository.entity.Address;
import com.unicorn.edu.eshop2.repository.entity.Customer;
import com.unicorn.edu.eshop2.service.ServiceLocator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EntityManagerMain {

    public static void main(String[] args) {
        EntityManager entityManager = ServiceLocator.createEntityManager();
        entityManager.getTransaction().begin();

        Customer customer = new Customer();
        customer.setEmail("aaa");
        customer.setFirstName("Aaa");
        customer.setLastName("Bbb");
        customer.setRegistrationDate(Calendar.getInstance());

        Address address = new Address();
        address.setCity("Pardubice");
        address.setPostalCode("530 09");
        address.setStreetName("Ulice");
        address.setStreetNumber("10/3");

        address.setCustomer(customer);
        List<Address> addresses = new ArrayList<Address>();
        addresses.add(address);
        customer.setAddresses(addresses);

        entityManager.persist(customer);
        //entityManager.persist(address);

        entityManager.getTransaction().commit();

        entityManager.close();
        ServiceLocator.shutdown();
    }
}
