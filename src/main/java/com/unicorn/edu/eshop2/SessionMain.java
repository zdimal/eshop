package com.unicorn.edu.eshop2;

import com.unicorn.edu.eshop2.repository.entity.Address;
import com.unicorn.edu.eshop2.service.ServiceLocator;
import org.hibernate.Session;

public class SessionMain {

    public static void main(String[] args) {
        Session session = ServiceLocator.createSession();
        session.beginTransaction();

        /*Address address = new Address();
        address.setCity("Pardubice");
        address.setPostalCode("530 09");
        address.setStreetName("Ulice");
        address.setStreetNumber("10/3");

        session.persist(address);*/
        Address address = session.get(Address.class, 1l);
        System.out.println(address);
        address.setCity("Chrudim");
        session.merge(address);


        session.getTransaction().commit();

        session.close();
        ServiceLocator.shutdown();
    }
}
