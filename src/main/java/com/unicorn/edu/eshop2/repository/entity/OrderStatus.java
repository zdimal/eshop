package com.unicorn.edu.eshop2.repository.entity;

public enum OrderStatus {

    CREATED, IN_PROGRESS, COMPLETED, CANCELLED
}
