package com.unicorn.edu.eshop2.repository.entity;

public enum AddressType {

    BILLING, CONTRACTUAL
}
